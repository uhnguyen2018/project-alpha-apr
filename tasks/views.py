from distutils.log import Log
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from tasks.models import Task
from django.views.generic import (
    CreateView,
    ListView,
    UpdateView,
)
from django.contrib.auth.mixins import LoginRequiredMixin


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "tasks/create.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()

        return redirect("show_project", pk=item.id)

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.id])


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/list.html"
    context_object_name = "tasklist"


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    template_name = "tasks/edit.html"
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")
